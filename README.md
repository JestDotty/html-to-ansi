`npm i --save html-to-ansi`

```js
const translate = require('html-to-ansi')
let output = translate('abc<br>abc') //abc\nabc
```

works with:
- `br`
- `font color='#123123'`
- removes all other unknown tags
