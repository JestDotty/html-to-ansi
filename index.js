const htmler = require('fast-html-parser')
const chalk = require('chalk')

const convert = data=>{
	let v = data.childNodes && data.childNodes.length?
		data.childNodes.map(d=> convert(d)).join(''):
		data.rawText
	switch(data.tagName){
		case 'br':
			v += '\n'
			break
		case 'font':
			v = chalk.hex(data.attributes.color)(v)
			break
	}
	return v
}
module.exports = txt=> convert(htmler.parse(`<p>${txt}</p>`))
